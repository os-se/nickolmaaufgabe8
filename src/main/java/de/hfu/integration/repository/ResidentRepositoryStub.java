package de.hfu.integration.repository;

import de.hfu.integration.domain.Resident;

import java.util.ArrayList;
import java.util.List;

public class ResidentRepositoryStub implements ResidentRepository{
    private ArrayList<Resident> residents = new ArrayList<Resident>();

    public void addResident(Resident r){
        residents.add(r);
    }
    @Override
    public List<Resident> getResidents(){
        return residents;    }
};
