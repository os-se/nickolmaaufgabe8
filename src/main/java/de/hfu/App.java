package de.hfu;

import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main( String[] args ) {
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        input = input.toUpperCase();
        System.out.println(input);
    }
}