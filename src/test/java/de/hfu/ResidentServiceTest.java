package de.hfu;
import de.hfu.integration.*;
import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.repository.ResidentRepositoryStub;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.easymock.EasyMock.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ResidentServiceTest {
    /**
     * Wird einmal vor allen Tests dieser Klasse aufgerufen
     */
    @BeforeAll
    static void initAll() {
        ResidentRepositoryStub repstub = new ResidentRepositoryStub();
        repstub.addResident(new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2)));
        repstub.addResident(new Resident("Alex","Müller","Musterstraße","Villingen",new Date(2009,3,22)));
        ResidentService service = new BaseResidentService(repstub);

    }

    /**
    * Wird ein einzigartiger Bewohner gefunden wenn er nur einmal vorhanden ist
     */
    @Test
    public void getUniqueResidentTest1() throws ResidentServiceException {
        ResidentRepositoryStub repstub = new ResidentRepositoryStub();
        repstub.addResident(new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2)));
        repstub.addResident(new Resident("Alex","Müller","Musterstraße","Villingen",new Date(2009,3,22)));
        ResidentService service = new BaseResidentService(repstub);

        Resident gesucht = new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2));
        Resident gefunden = service.getUniqueResident(gesucht);
        assert(gesucht.getGivenName().equals(gefunden.getGivenName()));
        assert(gesucht.getFamilyName().equals(gefunden.getFamilyName()));
        assert(gesucht.getStreet().equals(gefunden.getStreet()));
        assert(gesucht.getCity().equals(gefunden.getCity()));
        assert(gesucht.getDateOfBirth().equals(gefunden.getDateOfBirth()));
    }
    /**
     * Exeption wenn nicht einzigartig
     */
    @Test
    public void getUniqueResidentTest2() throws ResidentServiceException {
        ResidentRepositoryStub repstub = new ResidentRepositoryStub();
        repstub.addResident(new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2)));
        repstub.addResident(new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2)));
        ResidentService service = new BaseResidentService(repstub);

        Resident gesucht = new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2));

        assertThrows(ResidentServiceException.class,()->{service.getUniqueResident(gesucht);},"Bewohner nicht eindeutig zuordnenbar");
    }
    /**
     * Exeption wegen Wildcard
     */
    @Test
    public void getUniqueResidentWildcardTest(){
        ResidentRepositoryStub repstub = new ResidentRepositoryStub();
        repstub.addResident(new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2)));
        repstub.addResident(new Resident("Alex","Müller","Musterstraße","Villingen",new Date(2009,3,22)));
        ResidentService service = new BaseResidentService(repstub);

        Resident gesucht = new Resident("T*","Turner","Heerstraße","Stuttgart",new Date(20002,12,2));

        assertThrows(ResidentServiceException.class,()->{service.getUniqueResident(gesucht);},"Wildcard wurde obwohl verboten zugelassen");

    }
    /**
     * Nur ein Bewohner enspricht den Suchkriterien
     */
    @Test
    public void getFilteredResidentsList1(){
        ResidentRepositoryStub repstub = new ResidentRepositoryStub();
        repstub.addResident(new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2)));
        repstub.addResident(new Resident("Alex","Müller","Musterstraße","Villingen",new Date(2009,3,22)));
        ResidentService service = new BaseResidentService(repstub);

        Resident gesucht = new Resident("T*","*","*","Stuttgart",null);
        List<Resident> gefunden = service.getFilteredResidentsList(gesucht);

            assert(gefunden.get(0).getGivenName().equals("Tina"));
            assert(gefunden.get(0).getCity().equals("Stuttgart"));


    }

    /**
     * Mehrere Bewohner ensprechen den Suchkriterien
     */
    @Test
    public void getFilteredResidentsList2(){
        ResidentRepositoryStub repstub = new ResidentRepositoryStub();
        repstub.addResident(new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2)));
        repstub.addResident(new Resident("Tim","Müller","Musterstraße","Stuttgart",new Date(2002,12,2)));
        ResidentService service = new BaseResidentService(repstub);

        Resident gesucht = new Resident("T*","*","*","Stuttgart",null);
        List<Resident> gefunden = service.getFilteredResidentsList(gesucht);

                assert(gefunden.get(0).getGivenName()==("Tina"));
                assert(gefunden.get(0).getCity()==("Stuttgart"));

                assert(gefunden.get(1).getGivenName()==("Tim"));
                assert(gefunden.get(1).getCity()==("Stuttgart"));

        }
    /**
     * Kein Bewohner entspricht den Suchkriterien
     */
    @Test
    public void getFilteredResidentsList3(){
        ResidentRepositoryStub repstub = new ResidentRepositoryStub();
        repstub.addResident(new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2)));
        repstub.addResident(new Resident("Tim","Müller","Musterstraße","Stuttgart",new Date(2002,12,2)));
        ResidentService service = new BaseResidentService(repstub);

        Resident gesucht = new Resident("M*","*","*","Stuttgart",null);
        List<Resident> gefunden = service.getFilteredResidentsList(gesucht);

        assert(gefunden.isEmpty());

    }
    /**
     * Test mit Easy Mock
     */
    @Test
    public void getUniqueResidentMockTest() throws ResidentServiceException {
        List residents = new ArrayList<>();
        residents.add(new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2)));
        residents.add(new Resident("Tim","Müller","Musterstraße","Stuttgart",new Date(2002,12,2)));

        ResidentRepository ResidentRepMock = createMock(ResidentRepository.class);
        expect(ResidentRepMock.getResidents()).andReturn(residents);
        replay(ResidentRepMock);
        BaseResidentService service = new BaseResidentService(ResidentRepMock);
        Resident gesucht = new Resident("Tina","Turner","Heerstraße","Stuttgart",new Date(20002,12,2));
        Resident gefunden = service.getUniqueResident(gesucht);
        assert(gesucht.getGivenName().equals(gefunden.getGivenName()));
        assert(gesucht.getFamilyName().equals(gefunden.getFamilyName()));
        assert(gesucht.getStreet().equals(gefunden.getStreet()));
        assert(gesucht.getCity().equals(gefunden.getCity()));
        assert(gesucht.getDateOfBirth().equals(gefunden.getDateOfBirth()));

    }
    }

