package de.hfu;

import org.junit.jupiter.api.*;
import de.hfu.unit.Util;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Beispiele für Unit-Tests
 */
@DisplayName("Test der Klasse Monat")
public class MonatTest
{

    /**
     * Wird einmal vor allen Tests dieser Klasse aufgerufen
     */
    @BeforeAll
    static void initAll() {
    }

    /**
     * Wird jeweils vor den Tests dieser Klasse aufgerufen
     */
    @BeforeEach
    void init() {
    }

    /**
     * Monat ist ungültig: <0 oder größer 12
     */
    @Test
    void monthIsNotValidTest1() {
        assertThrows(IllegalArgumentException.class,()->{Util.istErstesHalbjahr(-1);},"Test throwt exeption, alles super");

    }
    @Test
    void monthIsNotValidTest2() {
        assertThrows(IllegalArgumentException.class,()->{Util.istErstesHalbjahr(13);},"Test throwt exeption, alles super");

    }
    /**
     * Tests mit gültigen Monaten
     */
    @Test
    void monthIsValidTest1(){
       assertTrue(Util.istErstesHalbjahr(1));
    }
    @Test
    void monthIsValidTest2(){
        assertFalse(Util.istErstesHalbjahr(12));
    }
    @Test
    void monthIsValidTest3(){
        assertFalse(Util.istErstesHalbjahr(7));
    }
    @Test
    void monthIsValidTest4(){
        assertTrue(Util.istErstesHalbjahr(6));
    }


    /**
     * Wird jeweils nach den Tests dieser Klasse aufgerufen
     */
    @AfterEach
    void tearDown() {
    }

    /**
     * Wird einmal nach allen Tests dieser Klasse aufgerufen
     */
    @AfterAll
    static void tearDownAll() {
    }}


