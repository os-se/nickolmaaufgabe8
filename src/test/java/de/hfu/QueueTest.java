package de.hfu;
import org.junit.jupiter.api.*;
import de.hfu.unit.Util;
import de.hfu.unit.Queue;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test der Klasse Queue")
public class QueueTest
{

    /**
     * Wird einmal vor allen Tests dieser Klasse aufgerufen
     */
    @BeforeAll
    static void initAll() {
    }

    /**
     * Wird jeweils vor den Tests dieser Klasse aufgerufen
     */
    @BeforeEach
    void init() {
    }


    /**
     * Queue nicht voll
     */
    @Test
    void enqueueDequeueTest1(){
       Queue queue = new Queue(3);
       queue.enqueue(1);
       queue.enqueue(2);
        assertEquals(1, queue.dequeue());
    }
    /**
     * Queue voll, letztes Element überschrieben
     */
    @Test
    void enqueueDequeueTest2(){
        Queue queue = new Queue(3);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.dequeue();
        queue.dequeue();
        assertEquals(4, queue.dequeue());
    }
    /**
     * Queue leer, dequeue schmeißt Exeption
     */
    @Test
    void dequeueTest(){
        Queue queue = new Queue(3);
        assertThrows(IllegalStateException.class,()->{queue.dequeue();},"dequeue schmeißt exeption");
    }
    /**
     * Queue mit illegalen Werten init
     */
    @Test
    void queueInitFailTest(){

        assertThrows(IllegalArgumentException.class,()->{new Queue(0);},"Queue wird nicht erstellt");
    }
    /**
     * Wird der Speicherplatz ringförmig genutzt
     */
    @Test
    void queueSpeicherRingTest() {
        Queue queue = new Queue(3);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.dequeue();
        queue.enqueue(4);
        assertEquals(2,queue.dequeue());
        assertEquals(3,queue.dequeue());
        assertEquals(4,queue.dequeue());
    }
    /**
     * Wird jeweils nach den Tests dieser Klasse aufgerufen
     */
    @AfterEach
    void tearDown() {
    }

    /**
     * Wird einmal nach allen Tests dieser Klasse aufgerufen
     */
    @AfterAll
    static void tearDownAll() {
    }}



